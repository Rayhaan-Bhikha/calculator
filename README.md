# Calculator
Calculator written in Java which takes a `String` and computes an answer by implementing the Shunting Yard Algorithm and using Reverse Polish Notation.

## Build and Run
### Linux
```
./gradlew run
```
### Windows
```
./gradlew.bat run
```

## Description
```java
// Normal mode
Calculator calculator = new Calculator();
// Verbose mode
Calculator calculator = new Calculator(true);
```

To obtain the evaluated expression call the following method.

```java
calculator.evaluateExpression(expression); // expression is type String 
```

## Example
```java
Calculator calculator = new Calculator();
calculator.evaluateExpression("((15/(7-(1+1)))*3)−(2+(1+1))"); // Answer 5.0
```
